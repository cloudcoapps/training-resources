//childComponentController.js
({
     fireComponentEvent : function(component, event, helper) {
         //Find event
         var msgFrmEvent = component.getEvent("messageEvent");
         
         //Set event parameter
         msgFrmEvent.setParams({"message": "Message passing from child component."});  
         
         //Fire event
         msgFrmEvent.fire();
     }
})
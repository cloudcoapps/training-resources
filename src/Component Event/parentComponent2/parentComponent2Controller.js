//parentComponentController.js
({
     checkChildMessage : function(component, event, helper) {
         //Fetch event message set in child component.
         var eventMsgFrmChild  = event.getParam("message");
         
         //Set event message to display in UI.
         component.set("v.messageFromEvent", eventMsgFrmChild);
     }
})
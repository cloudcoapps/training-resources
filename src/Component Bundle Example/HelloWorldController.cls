public with sharing class HelloWorldController {
	
    @AuraEnabled
	public static User getCurrentUser() {
        User u = [select id,FirstName,Email from user where id = : UserInfo.getUserId()];
        
		return u;
	}
}
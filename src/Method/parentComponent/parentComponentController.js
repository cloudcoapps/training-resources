//js controller for parent component
({
     callChildMethod : function(component, event, helper) {
         //Child aura method calling logic.       
         var childCmp = component.find("childComp");
         var rtnMsgFromChildMethod = childCmp.messageFromChild('parameter 1','parameter 2');

         //Set local attribute with string value, comming from child aura method.
         component.set("v.messageFromChild", rtnMsgFromChildMethod);
     }
})